package com.tinklab.tinklab;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.concurrent.CountDownLatch;

/**
 * Created by William_ho on 2018/2/23.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    private CountDownLatch mLock;
    @Rule
    public ActivityTestRule<MainActivity> mTestActivity = new ActivityTestRule<MainActivity>(MainActivity.class, false, false){
        @Override
        protected void beforeActivityLaunched() {
            super.beforeActivityLaunched();
            mLock = new CountDownLatch(1);
        }

        @Override
        protected void afterActivityLaunched() {
            super.afterActivityLaunched();
            mLock.countDown();
        }
    };

    private void waitForUI(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void tabClick() {
        mTestActivity.launchActivity(new Intent(Intent.ACTION_MAIN));
        waitLock();
        waitForUI(1000L);
        Espresso
                .onView(Matchers.allOf(ViewMatchers.withText("SHOP"), ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.tabs))))
                .perform(ViewActions.click());
        waitForUI(500);
        Espresso
                .onView(Matchers.allOf(ViewMatchers.withText("EAT"), ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.tabs))))
                .perform(ViewActions.click());
        waitForUI(500); // Wait a little until the content is loaded
        Espresso
                .onView(Matchers.allOf(ViewMatchers.withText("CITY GUIDE"), ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.tabs))))
                .perform(ViewActions.click());
        mTestActivity.getActivity().finish();
    }

    @Test
    public void pagerSwipe() {
        mTestActivity.launchActivity(new Intent(Intent.ACTION_MAIN));
        waitLock();
        waitForUI(1000L);
        Espresso
                .onView(ViewMatchers.withId(R.id.pager))
                .perform(ViewActions.swipeLeft());
        waitForUI(500L);
        Espresso
                .onView(ViewMatchers.withId(R.id.pager))
                .perform(ViewActions.swipeLeft());
        waitForUI(500L);
        Espresso
                .onView(ViewMatchers.withId(R.id.pager))
                .perform(ViewActions.swipeRight());
        waitForUI(500L);
        Espresso
                .onView(ViewMatchers.withId(R.id.pager))
                .perform(ViewActions.swipeRight());
        mTestActivity.getActivity().finish();
    }

    @Test
    public void cityGuideViewScrollDown() {
        mTestActivity.launchActivity(new Intent(Intent.ACTION_MAIN));
        waitLock();
        waitForUI(3000L);
        Espresso
                .onView(Matchers.allOf(ViewMatchers.withId(R.id.recyclerView), ViewMatchers.isCompletelyDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(14));
        waitForUI(500L);
        mTestActivity.getActivity().finish();
    }

    private void waitLock(){
        try{
            mLock.await();
        }catch(Exception e){
        }
    }
}
