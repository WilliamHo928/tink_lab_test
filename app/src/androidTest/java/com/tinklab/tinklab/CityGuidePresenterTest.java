package com.tinklab.tinklab;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.tinklab.tinklab.datamodel.CityGuideItem;
import com.tinklab.tinklab.model.CityGuideDataManager;
import com.tinklab.tinklab.presenter.CityGuidePresenter;
import com.tinklab.tinklab.ui.PageCityGuide;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static com.tinklab.tinklab.model.CityGuideDataManager.*;

/**
 * Created by William_ho on 2018/2/23.
 */

public class CityGuidePresenterTest {
    Context appContext = InstrumentationRegistry.getTargetContext();

    @Test
    public void requestData() throws Exception {
        CityGuideDataManager mockCityGuideDataManager = Mockito.mock(CityGuideDataManager.class);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                //这里可以获得传给performLogin的参数
                Object[] arguments = invocation.getArguments();

                //callback是第一个参数
                CityGuideCallback callback = (CityGuideCallback) arguments[0];

                List<CityGuideItem> mockList = new ArrayList<>();
                CityGuideItem mockItem = new CityGuideItem();
                mockItem.setType(1);
                mockItem.setTitle("this is mock data");

                mockList.add(mockItem);
                callback.onComplete(mockList);

                return mockList;
            }
        }).when(mockCityGuideDataManager).requestData(Mockito.mock(CityGuideCallback.class));

        CityGuidePresenter presenter = new CityGuidePresenter(appContext, Mockito.mock(PageCityGuide.class));
        presenter.handlePageEnter();
    }
}
