package com.tinklab.tinklab.model;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.tinklab.tinklab.datamodel.CityGuideItem;
import com.tinklab.tinklab.utils.CityGuideUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by William_ho on 2018/2/22.
 */

public class CityGuideDataManager {
    private static CityGuideDataManager mInstance;
    private Context mContext;
    private String mDefaultJson = "";

    private CityGuideDataManager(Context context) {
        mContext = context;
    }

    public synchronized static CityGuideDataManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CityGuideDataManager(context);
        }
        return mInstance;
    }

    private ExecutorService mExecutorService = new ThreadPoolExecutor(1, 1, 60,
            TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(8), new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "FetchCityGuideDataThread");
            // Change thread priority to background.
            t.setPriority(Thread.NORM_PRIORITY - 1);
            return t;
        }
    }, new ThreadPoolExecutor.DiscardOldestPolicy());

    public class FetchCityGuideDataRunnable implements Runnable {
        private CityGuideCallback mCallback;

        public FetchCityGuideDataRunnable(CityGuideCallback cb) {
            mCallback = cb;
        }

        @Override
        public void run() {
            if (TextUtils.isEmpty(mDefaultJson)) {
                mDefaultJson = CityGuideUtil.getDefaultDataList(mContext, CityGuideUtil.DEFAULT_DATA_FILE_NAME);
            }
            if (TextUtils.isEmpty(mDefaultJson)) {
                mCallback.onError();
                return;
            }
            ArrayList<CityGuideItem> referenceItems = new ArrayList<>();
            try {
                JSONObject obj = new JSONObject(mDefaultJson);
                JSONArray posListJsonArray = obj.getJSONArray(CityGuideUtil.KEY_CITY_GUIDE_LIST);
                for (int i = 0; i < posListJsonArray.length(); ++i) {
                    JSONObject posListObject = posListJsonArray.getJSONObject(i);
                    CityGuideItem referenceItem = CityGuideUtil.parseData(posListObject);
                    referenceItems.add(referenceItem);
                }
                ArrayList<CityGuideItem> items = new ArrayList<>();
                for (int i=0; i<15; i++) {
                    CityGuideItem item = referenceItems.get((int)(Math.random()*10%4));
                    items.add(item);
                }
                mCallback.onComplete(items);
            }catch (Exception e) {
                mCallback.onError();
            }
        }
    }

    public void requestData(CityGuideCallback cb) {
        if (!mExecutorService.isShutdown()) {
            mExecutorService.execute(new FetchCityGuideDataRunnable(cb));
        }
    }

    public interface CityGuideCallback {
        void onComplete(List<CityGuideItem> list);
        void onError();
    }
}
