package com.tinklab.tinklab.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tinklab.tinklab.R;
import com.tinklab.tinklab.datamodel.CityGuideItem;

import java.util.List;

/**
 * Created by William_ho on 2018/2/21.
 */

public class CityGuideDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CityGuideItem> mItemList;

    //
    // View Holder Definitions
    //
    private class ImageOnlyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        public ImageOnlyViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
        }
    }

    private class ImageAndContentViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView description;
        public ImageAndContentViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            title = (TextView) v.findViewById(R.id.title);
            description = (TextView) v.findViewById(R.id.description);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder vh;
        switch (viewType) {
            case CityGuideItem.ITEM_IMAGE_ONLY:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.city_guide_item_type_a, parent, false);
                vh = new ImageOnlyViewHolder(v);
                break;
            case CityGuideItem.ITEM_IMAGE_TITLE_AND_CONTENT:
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.city_guide_item_type_b, parent, false);
                vh = new ImageAndContentViewHolder(v);
                break;
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CityGuideItem item = mItemList.get(position);
        switch (item.getType()) {
            case CityGuideItem.ITEM_IMAGE_ONLY:
                // todo : apply imageLoader to load image, OkHttp, Glide
                if ("big_a".equals(item.getImageUrl())) {
                    ((ImageOnlyViewHolder) holder).image.setImageResource(R.drawable.tink_default_big);
                } else {
                    ((ImageOnlyViewHolder) holder).image.setImageResource(R.drawable.tink_default_big);
                }
                break;
            case CityGuideItem.ITEM_IMAGE_TITLE_AND_CONTENT:
            default:
                // todo : apply imageLoader to load image, OkHttp, Glide
                if ("small_a".equals(item.getImageUrl())) {
                    ((ImageAndContentViewHolder) holder).image.setImageResource(R.drawable.tink_default_small);
                } else {
                    ((ImageAndContentViewHolder) holder).image.setImageResource(R.drawable.tink_default_small);
                }
                ((ImageAndContentViewHolder) holder).title.setText(item.getTitle());
                ((ImageAndContentViewHolder) holder).description.setText(item.getDescription());
                break;
        }

    }

    @Override
    public int getItemCount() {
        if (mItemList != null) {
            return mItemList.size();
        }
        return 0;
    }

    public void setData(List<CityGuideItem> list) {
        mItemList = list;
        notifyDataSetChanged();
    }

    public void loadMore(List<CityGuideItem> list) {
        mItemList.addAll(list);
        notifyDataSetChanged();
    }
}
