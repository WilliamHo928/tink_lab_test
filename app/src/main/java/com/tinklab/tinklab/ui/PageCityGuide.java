package com.tinklab.tinklab.ui;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tinklab.tinklab.R;
import com.tinklab.tinklab.datamodel.CityGuideItem;
import com.tinklab.tinklab.presenter.CityGuidePresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by William_ho on 2018/2/21.
 */

public class PageCityGuide extends PageView implements ICityGuideView {

    private RecyclerView mRecyclerView;
    private CityGuideDataAdapter mAdapter;
    private CityGuidePresenter mPresenter;
    private boolean mIsloading = false;

    public PageCityGuide(Context context) {
        super(context);
        initViews(context);
        mPresenter = new CityGuidePresenter(context, this);
        mPresenter.handlePageEnter();
    }

    private void initViews(final Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.page_content, null);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mAdapter = new CityGuideDataAdapter();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(VISIBLE);
        mRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!mIsloading) {
                    Toast.makeText(context, "loading", Toast.LENGTH_SHORT).show();
                    mPresenter.handleLoadMore(current_page);
                    mIsloading = true;
                }
            }
        });
        addView(view);
    }

    @Override
    public void refresh() {
    }

    @Override
    public void setData(List<CityGuideItem> items) {
        if (mAdapter != null) {
            mAdapter.setData(items);
        }
    }

    @Override
    public void loadMore(List<CityGuideItem> items) {
        if (mAdapter != null) {
            mAdapter.loadMore(items);
        }
        mIsloading = false;
    }
}
