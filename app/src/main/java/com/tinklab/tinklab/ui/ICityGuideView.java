package com.tinklab.tinklab.ui;

import com.tinklab.tinklab.datamodel.CityGuideItem;

import java.util.List;

/**
 * Created by William_ho on 2018/2/22.
 */

public interface ICityGuideView {
    void setData(List<CityGuideItem> items);
    void loadMore(List<CityGuideItem> items);
}
