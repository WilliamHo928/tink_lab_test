package com.tinklab.tinklab.ui;

import android.content.Context;
import android.widget.RelativeLayout;

/**
 * Created by William_ho on 2018/2/21.
 */

public abstract class PageView extends RelativeLayout {
    public PageView(Context context) {
        super(context);
    }
    public abstract void refresh();
}
