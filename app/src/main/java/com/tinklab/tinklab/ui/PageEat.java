package com.tinklab.tinklab.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tinklab.tinklab.R;

/**
 * Created by William_ho on 2018/2/21.
 */

public class PageEat extends PageView {
    public PageEat(Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.page_content, null);
        TextView textView = (TextView) view.findViewById(R.id.text);
        textView.setText("Page Eat");
        textView.setVisibility(VISIBLE);
        addView(view);
    }

    @Override
    public void refresh() {

    }
}
