package com.tinklab.tinklab.datamodel;

/**
 * Created by William_ho on 2018/2/21.
 */

public class CityGuideItem {

    public static final int ITEM_IMAGE_ONLY = 0;
    public static final int ITEM_IMAGE_TITLE_AND_CONTENT = 1;

    private int mType;
    private String mImageUrl;
    private String mTitle;
    private String mDescription;

    /*
    public CityGuideItem(int type, String imageUrl, String title, String description) {
        mType = type;
        mImageUrl = imageUrl;
        mTitle = title;
        mDescription = description;
    }
    */

    public CityGuideItem(){}

    public void setType(int type) { mType = type; }

    public int getType() { return mType; }

    public void setImageUrl(String url) { mImageUrl = url; }

    public String getImageUrl() { return mImageUrl; }

    public void setTitle(String title) { mTitle = title; }

    public String getTitle() { return mTitle; }

    public void setDescription(String description) { mDescription = description; }

    public String getDescription() { return mDescription; }
}
