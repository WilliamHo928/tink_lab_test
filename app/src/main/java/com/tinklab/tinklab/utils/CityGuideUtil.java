package com.tinklab.tinklab.utils;

import android.content.Context;
import android.content.res.AssetManager;

import com.tinklab.tinklab.datamodel.CityGuideItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by William_ho on 2018/2/22.
 */

public class CityGuideUtil {

    public static final String DEFAULT_DATA_FILE_NAME = "local_data.json";

    public static final String KEY_CITY_GUIDE_LIST = "city_guide_list";
    private static final String KEY_TYPE = "type";
    private static final String KEY_IMAGE_URL = "image_url";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";

    public static String getDefaultDataList(Context context, String fileName){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public static CityGuideItem parseData(JSONObject object) throws JSONException {
        CityGuideItem item = new CityGuideItem();
        item.setType(object.getInt(KEY_TYPE));
        item.setImageUrl(object.getString(KEY_IMAGE_URL));
        item.setTitle(object.getString(KEY_TITLE));
        item.setDescription(object.getString(KEY_DESCRIPTION));
        return item;
    }
}
