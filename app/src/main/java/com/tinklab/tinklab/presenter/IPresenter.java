package com.tinklab.tinklab.presenter;

/**
 * Created by William_ho on 2018/2/22.
 */

public interface IPresenter {
    void handlePageEnter();
    void handleLoadMore(int page);
}
