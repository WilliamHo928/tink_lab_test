package com.tinklab.tinklab.presenter;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.tinklab.tinklab.datamodel.CityGuideItem;
import com.tinklab.tinklab.model.CityGuideDataManager;
import com.tinklab.tinklab.ui.ICityGuideView;

import java.util.List;

/**
 * Created by William_ho on 2018/2/22.
 */

public class CityGuidePresenter implements IPresenter {

    private Context mContext;
    private ICityGuideView mCityGuideView;

    private static final int MSG_UPDATE_LIST = 1;
    private static final int MSG_LOAD_MORE = 2;

    private Handler mHandler = new Handler(Looper.getMainLooper()){
        public void handleMessage(Message msg) {
            List<CityGuideItem> items;
            switch (msg.what) {
                case MSG_UPDATE_LIST:
                    items = (List<CityGuideItem>) msg.obj;
                    mCityGuideView.setData(items);
                    break;
                case MSG_LOAD_MORE:
                    items = (List<CityGuideItem>) msg.obj;
                    mCityGuideView.loadMore(items);
                default:
                    break;
            }
        }
    };

    public CityGuidePresenter(Context context, ICityGuideView cityGuideView) {
        mContext = context;
        mCityGuideView = cityGuideView;
    }

    @Override
    public void handlePageEnter() {
        CityGuideDataManager.getInstance(mContext).requestData(new CityGuideDataManager.CityGuideCallback() {

            @Override
            public void onComplete(List<CityGuideItem> list) {
                Message msg = Message.obtain();
                msg.what = MSG_UPDATE_LIST;
                msg.obj = list;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onError() {
                // todo : apply error handling
            }
        });
    }

    @Override
    public void handleLoadMore(int page) {
        CityGuideDataManager.getInstance(mContext).requestData(new CityGuideDataManager.CityGuideCallback() {

            @Override
            public void onComplete(List<CityGuideItem> list) {
                Message msg = Message.obtain();
                msg.what = MSG_LOAD_MORE;
                msg.obj = list;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onError() {
                // todo : apply error handling
            }
        });
    }
}
